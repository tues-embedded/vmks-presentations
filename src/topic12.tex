\documentclass[numbering=fraction]{beamer}

\usepackage[bulgarian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[style=alphabetic,backend=biber]{biblatex}
\usepackage{amsmath}
\usepackage{blindtext}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{colortbl}
\usepackage{amssymb}
\usepackage{listings}

\usetheme[progressbar=frametitle]{metropolis}

\addbibresource{bibliography.bib}

\uselanguage{Bulgarian}
\languagepath{Bulgarian}

\graphicspath{{pictures/}}

\title{Код на Hamming}
\titlegraphic{\vspace{4cm}\flushright\includegraphics[width=2cm,height=2cm]{tues-logo.png}}
\author{Кристиян Стоименов}
\institute{ТУЕС,\\\textbf{ПВМКС}}
\date{\today{}}

\begin{document}
\include{common}

\begin{frame}[plain]
\maketitle
\end{frame}

\begin{frame}[allowframebreaks]{Какво е код на Hamming?}
\begin{itemize}
	\item Метод за внасяне на ,,защитни'' битове, чрез които да маркираме
		съобщение, което изпращаме по такъв начин, че да можем да научим за
		възникване на до две грешки и да поправим една.
	\item Измислени от Richard Hamming през 40-те години в Bell Labs.
	\item Изпращаме съобщения с дължина $k = 2^r - r - 1$, добавяйки точно $r$
		,,защитни'' бита (т.нар. redundency).
	\item Това означава, че например, можем да боравим с 15-битови
		\textit{блокове} с 4 бита redundency (11-битови съобщения); или пък със
		7-битови блокове с 3 бита redundency (4-битови съобщения).
	\item Тези кодове са известни обичайно съответно като (15, 11, 3) код и
		(7, 4, 3) код.
	\item Разбира се, можем да използваме и по-големи блокове (напр. 256b), но
		поради свойствата му, способностите за брой поправими грешки остават
		само за единствен бит. Следователно вероятността да изпуснем възникали
		(четен брой) грешки е правопропорционална на големината на блока.
	\item Основната идея на кода на Hamming е заложена в \textit{проверката по
		четност} (parity bit).
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Проверка по четност}
\par Припомняме какво представляваше проверката по четност:
\begin{itemize}
	\item Уговаряме се да следваме правилото във всеки блок данни задължително
		броят на вдигнатите битове (1) да бъде четен.
	\item Така, ако блоковете ни са 8-битови с parity bit в края, то съобщението
		\verb#0110001# ще има parity bit \verb#1#, докато \verb#1111000# - \verb#0#.
	\item Това е механизмът, който използва \texttt{UART} интерфейса, в
		конфигурацията \verb#8E# (8-битови пакети, ,,четна'' четност).
\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{Код на Hamming - принципи}
\par Основната идея, която трябва да усвоите е следната: \textit{ако поставим
	достатъчно на брой parity проверки на съответните места, получаваме удобен и
	лесен начин за откриване на единствена грешка}.
\par Ще се опитаме да разберем (15, 11) кода, но принципът на действие се
	запазва при блок с произволна дължина $2^k$.

\break
\vspace*{0.5cm}
\begin{center}
\begin{tabular}{ |c|c|c|c| }
\hline \cellcolor{orange!25}0 & \cellcolor{yellow!25}1 & \cellcolor{yellow!25}2 & \cellcolor{green!25}3 \\
\hline \cellcolor{yellow!25}4 & \cellcolor{green!25}5  & \cellcolor{green!25}6	& \cellcolor{green!25}7 \\
\hline \cellcolor{yellow!25}8 & \cellcolor{green!25}9  & \cellcolor{green!25}10 & \cellcolor{green!25}11 \\
\hline \cellcolor{green!25}12 & \cellcolor{green!25}13 & \cellcolor{green!25}14 & \cellcolor{green!25}15 \\
\hline
\end{tabular}
\end{center}

\par Използваме този наглед нагласен ,,формат'', попълвайки съобщението, което
	искаме да изпратим (11b) в зелените клетки. В жълтите ще поставим redundency
	битовете, а оражевото временно ще игнорираме.
\par Стойностите, поставени вътре в шаблона, отговарят на ,,индекса'', който ще
	използваме за съответната клетка (т.е. аналогично можем да си представим и
	последователно разположение, вместо таблично).

\break
\par Нека попълним клетките със съобщението \texttt{10011001101}:
\begin{center}
\begin{tabular}{ |c|c|c|c| }
\hline \cellcolor{orange!25} & \cellcolor{yellow!25} & \cellcolor{yellow!25} & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25} & \cellcolor{green!25}0 & \cellcolor{green!25}0 & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25} & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}0 \\
\hline \cellcolor{green!25}1 & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}1 \\
\hline
\end{tabular}
\end{center}

\par Жълтите клетки (redundency битовете) попълваме с parity стойности за
	следните четири подгрупи от клетки:

\fboxsep=0pt
\noindent
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{white!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{white!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{white!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{white!25} & \cellcolor{blue!25} \\
		\hline
	\end{tabular}
\end{minipage}
\hfill%
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline
	\end{tabular}
\end{minipage}
\hfill%
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} \\
		\hline \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} \\
		\hline \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline
	\end{tabular}
\end{minipage}
\hfill%
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} \\
		\hline \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} & \cellcolor{white!25} \\
		\hline \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} & \cellcolor{blue!25} \\
		\hline
	\end{tabular}
\end{minipage}

\break
\par Забележете, че първите две подгрупи успяват да намерят колоната, в която е
	възникнала грешка, докато вторите две - редичката.
\par Нека определим стойностите на redundency битовете за примерното съобщение: \\

\vspace{.5cm}
\fboxsep=0pt
\noindent
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{blue!25}  & \cellcolor{white!25}  & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}  & \cellcolor{blue!25}0 & \cellcolor{white!25}0 & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}  & \cellcolor{blue!25}1 & \cellcolor{white!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{white!25}1 & \cellcolor{blue!25}1 & \cellcolor{white!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
\end{minipage}
\hfill%
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{white!25}  & \cellcolor{blue!25}  & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}  & \cellcolor{white!25}0 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}  & \cellcolor{white!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{white!25}1 & \cellcolor{white!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
\end{minipage}
\hfill
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{white!25}  & \cellcolor{white!25}  & \cellcolor{white!25}1 \\
		\hline \cellcolor{blue!25}  & \cellcolor{blue!25}0 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}  & \cellcolor{white!25}1 & \cellcolor{white!25}0 & \cellcolor{white!25}0 \\
		\hline \cellcolor{blue!25}1 & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
\end{minipage}
\hfill
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{white!25}  & \cellcolor{white!25}  & \cellcolor{white!25}1 \\
		\hline \cellcolor{white!25} & \cellcolor{white!25}0 & \cellcolor{white!25}0 & \cellcolor{white!25}1 \\
		\hline \cellcolor{blue!25}  & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{blue!25}1 & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
\end{minipage}

\break
\par Както може би забелязвате, интересното за позициите на redundency битовете
	(и причината за именно този избор), е това, че всеки redundency бит попада в
	точно една от подгрупи.

\break
\par Друго важно наблюдение ни показва по какъв начин избрахме точно това да са
	подгрупите - ако разгледаме отново шаблона за попълване с въведени индексите
	на всяка от клетките ще забележим, че например първата подгрупа включва тези
	и само тези клетки, чиито индески завършват на 1.
\begin{center}
\begin{tabular}{ |c|c|c|c| }
\hline \cellcolor{white!25}0000  & \cellcolor{blue!25}0001  & \cellcolor{white!25}0010  & \cellcolor{blue!25}0011 \\
\hline \cellcolor{white!25}0100  & \cellcolor{blue!25}0101  & \cellcolor{white!25}0110  & \cellcolor{blue!25}0111 \\
\hline \cellcolor{white!25}1000  & \cellcolor{blue!25}1001  & \cellcolor{white!25}1010  & \cellcolor{blue!25}1011 \\
\hline \cellcolor{white!25}1100  & \cellcolor{blue!25}1011  & \cellcolor{white!25}1110  & \cellcolor{blue!25}1111 \\
\hline
\end{tabular}
\end{center}

\par Аналогично следващите подгрупи ни дават тези, включващи \texttt{..1.},
\texttt{.1..} и \texttt{1...}.

\break
\par Така до момента можем да попълним всички без един от битовете в онзи
16-битов пакет:
\begin{center}
\begin{tabular}{ |c|c|c|c| }
\hline \cellcolor{orange!25} & \cellcolor{yellow!25}1 & \cellcolor{yellow!25}1 & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25}0 & \cellcolor{green!25}0 & \cellcolor{green!25}0 & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25}0 & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}0 \\
\hline \cellcolor{green!25}1 & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}1 \\
\hline
\end{tabular}
\end{center}

\par Нека го игнорираме още малко и да разгледаме как точно поправяме възникнала
грешка.
\par \emph{Бележка: До момента можем да кажем, че сме се запознали с (почти
цялата) процедура по кодиране и следва да декодираме.}

\break
\par Допускаме, че бит с индекс 6 е бил обърнат.
\begin{center}
\begin{tabular}{ |c|c|c|c| }
\hline \cellcolor{orange!25} & \cellcolor{yellow!25}1 & \cellcolor{yellow!25}1 & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25}0 & \cellcolor{green!25}0 &
	\cellcolor{green!25}\textcolor{red}{1} & \cellcolor{green!25}1 \\
\hline \cellcolor{yellow!25}0 & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}0 \\
\hline \cellcolor{green!25}1 & \cellcolor{green!25}1 & \cellcolor{green!25}0 & \cellcolor{green!25}1 \\
\hline
\end{tabular}
\end{center}

\par Извършваме проверките по четност, използвайки подгрупите, определящи
стойностите на redundency битовете една по една и записваме резултата от всяка
от тях като 0 или 1:
\vspace{.2cm}
\fboxsep=0pt
\noindent
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{blue!25}1 & \cellcolor{white!25}1 & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{blue!25}0 & \cellcolor{white!25}\textcolor{red}{1} & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{blue!25}1 & \cellcolor{white!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{white!25}1 & \cellcolor{blue!25}1 & \cellcolor{white!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
	\begin{center} \par 0 \end{center}
\end{minipage}
\hfill%
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{white!25}1 & \cellcolor{blue!25}1 & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{white!25}0 & \cellcolor{blue!25}\textcolor{red}{1} & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{white!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{white!25}1 & \cellcolor{white!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
	\begin{center} \par 1 \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25}  & \cellcolor{white!25}1 & \cellcolor{white!25}1 & \cellcolor{white!25}1 \\
		\hline \cellcolor{blue!25}0 & \cellcolor{blue!25}0 & \cellcolor{blue!25}\textcolor{red}{1} & \cellcolor{blue!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{white!25}1 & \cellcolor{white!25}0 & \cellcolor{white!25}0 \\
		\hline \cellcolor{blue!25}1 & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
	\begin{center} \par 1 \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{0.23\linewidth}
	\begin{tabular}{ |c|c|c|c| }
		\hline \cellcolor{white!25} & \cellcolor{white!25}1 & \cellcolor{white!25}1 & \cellcolor{white!25}1 \\
		\hline \cellcolor{white!25}0 & \cellcolor{white!25}0 & \cellcolor{white!25}\textcolor{red}{1} & \cellcolor{white!25}1 \\
		\hline \cellcolor{blue!25}0 & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}0 \\
		\hline \cellcolor{blue!25}1 & \cellcolor{blue!25}1 & \cellcolor{blue!25}0 & \cellcolor{blue!25}1 \\
		\hline
	\end{tabular}
	\begin{center} \par 0 \end{center}
\end{minipage}

\break
\par \textbf{Магия!} Получихме именно 6.
\par Основната причина този декодиращ алгоритъм да работи (и при това така
елегетно) се дължи на организацията на ,,подгрупите'' от битове, чрез които
ефективно извършваме двоично търсене - най-десният бит грешен ли е? а този
вляво? а този вляво? \dots а най-левият бит грешен ли е?

\break
\par Последното парче от пъзела е бит с индекс 0 - каква функция изпълнява той?
Всъщност той не е полезен за алгоритъма, който разгледахме до момента. Тъй като
обаче обичайно 15b блок данни е твърде необичайно (не е степен на 2), последният
бит се използва за проверка по четност на останалите 15 бита. По този начин
получаваме механизъм за поправяне на единствена грешка, но улавяне на две
възникнали. Тази добавка на още един бит за четност обичайно се нарича разширен
код на Hamming.

\break
\par \textit{Забележка: Вземете под внимание, че ако възникнат например 4 грешки
в единствен блок от данни, кодът на
Hamming няма да успее да помогне. Него използваме с допускането, че грешки има
не твърде често и не твърде много.}
\end{frame}

\begin{frame}[allowframebreaks]{Код на Hamming - обобщение}
\par \textbf{Кодираме} съобщение като за всеки един от битовете, съставляващи
	индексите, последователно изчислим бит за четност, a накрая добавим и такъв
	за целия блок.
\par \textbf{Декодираме} съобщение като последователно проверим всяка от
	подгрупите за грешка в четността и си отбележим резултатите от всяка
	проверка. Накрая събираме резултата и ако той не е 0, поправяме грешката в
	съответния индекс. Същевременно, проверките извършваме едва когато сме
	проверили ,,голямия'' parity бит.
\end{frame}

\begin{frame}[fragile]
\begin{block}{Задача}
\bigskip
\par Реализирайте следните функции, които да употребяват разширения (8, 4) Hamming код:
\vspace*{-.3cm}
\begin{verbatim}
	uint8_t hamming_encode(uint8_t);
	bool hamming_decode(uint8_t *data);
\end{verbatim}
\vspace*{-.2cm}
\par Кодирането трябва да приема 4-битово съобщение и да връща 8-битов блок,
	който да е поставил правилните стойности за redundency битовете.
\par Декодирането трябва да приема блок данни, кодирани посредством
	другата функция, и да запази изпратеното съобщение в долните 4 бита от
	\verb#*data#, поправяйки грешка, ако такава е възникнала.
\par Върнатата стойност трябва да съответства на резултата от проверката по
	четност спрямо разширения код на Hamming.
\end{block}
\end{frame}

\begin{frame}[fragile]
\begin{block}{Задача}
\bigskip
\par Използвайки функциите \verb#hamming_encode()# и \verb#hamming_decode()#,
	изпратете съобщение по I2C, което е защитено посредством разширения (8, 4)
	Hamming код.
\end{block}
\end{frame}

\begin{frame}{Общо за шумозащитните кодове (\textit{ECC})}
	\par Доста полезни, интересни и \textit{трудни}. Обединяват много дялове на точните науки.
\par Някои други популярни кодове, които има по-големи поправящи способности
	включват:
\begin{itemize}
	\item \textbf{Кодове на Reed-Muller} - имат много интересна интерпретация в
		контекста на крайни геометрии; използвани доста в космоса.
	\item \textbf{Кодове на Reed-Solomon} - CD, DVD, QR, \dots
	\item \textbf{BCH кодове} - използват се в памети;
	\item \textbf{Turbo кодове} - 3G, 4G, \dots
\end{itemize}
\end{frame}

\begin{frame}[noframenumbering,plain,allowframebreaks]
\section{Литература}
\begin{itemize}
\item \fullcite{3b1b_hamming_1}
\item \fullcite{3b1b_hamming_2}
\item \fullcite{ben_eater_hamming}
\end{itemize}
\end{frame}

\end{document}

