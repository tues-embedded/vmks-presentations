# ВМКС презентации

Презентации за предмета ПВМКС в ТУЕС.



# Инструкции за компилиране

**Зависимости**
```
// Arch
$ pacman -S texlive-basic texlive-latexrecommended biber
```

```
# Equivalent to `make -C src/ all` - compiles all separate
# *.tex files which reside src/
$ make -C src/

# Compiles a single presentation - e.g `topic1.tex`.
$ make -C src/ topic1
```

# Описание на темите

| Име | Презентации по теория | Упражнения |
| --- | --------------------- | ---------- |
| Преговор  | `topic1`, `topic2` | `prac1` |
| WS2812B LED ленти | `topic3` | `prac2`, `prac3` |
| Процесор, прекъсвания | `topic4`, `topic6` | `prac4` |
| Памет, портове | `topic5` | `prac4` |
| Таймери | `topic7` | |

